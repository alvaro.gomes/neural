package br.com.ajbg.util;

public class ElementoSimples {
	private double[] entradas;
	private double saida;

	public ElementoSimples(double[] e, double s) {
		entradas = e;
		saida = s;
	}

	public double[] getEntradas() {
		return entradas;
	}

	public double getSaida() {
		return saida;
	}
}

