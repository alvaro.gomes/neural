package br.com.ajbg.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

public class SolucaoSimples {
	public static List<ElementoSimples> load(InputStream is, int qtdIn)
			throws IOException {
		List<ElementoSimples> ret = new ArrayList<>();
		Scanner scan = new Scanner(is);
		while (scan.hasNextDouble()) {
			double[] entradas = new double[qtdIn];
			for (int i=0; i<qtdIn; i++) {
				entradas[i] = scan.nextDouble();
			}
			ret.add(new ElementoSimples(entradas, scan.nextDouble()));
		}
		return ret;
	}

	public static List<ElementoSimples> loadResource(String resource, int qtdIn)
			throws IOException {
		InputStream is = SolucaoSimples.class.getResourceAsStream(resource);
		List<ElementoSimples> ret = load(is, qtdIn);
		is.close();
		return ret;
	}

	public static List<ElementoSimples> loadFile(String file, int qtdIn)
			throws IOException {
		InputStream is = new FileInputStream(file);
		List<ElementoSimples> ret = load(is, qtdIn);
		is.close();
		return ret;
	}
}

