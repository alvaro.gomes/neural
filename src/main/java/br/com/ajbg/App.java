package br.com.ajbg;

import java.util.List;

import br.com.ajbg.neural.Neuronio;
import br.com.ajbg.neural.NeuronioDegrau;
import br.com.ajbg.util.ElementoSimples;
import br.com.ajbg.util.SolucaoSimples;

public class App {
	public static void main(String[] args) throws Exception {
		List<ElementoSimples> treino = SolucaoSimples.loadResource("/and.txt", 2);

		Neuronio n = new NeuronioDegrau(2);
		System.out.println("Antes:");
		for (ElementoSimples e : treino) {
			System.out.println(n.ativar(e.getEntradas()));
		}

		List<Double> erro = n.treinar(treino, 1, 0, 100);
		System.out.println("Depois:");
		for (ElementoSimples e : treino) {
			System.out.println(n.ativar(e.getEntradas()));
		}

		System.out.println("Curva de erro:");
		for (Double d : erro) {
			System.out.print(d + " ");
		}
		System.out.println();
	}
}

